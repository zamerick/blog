import Vue from "vue";
import moment from "moment";

Vue.filter("formatDate", function(date) {
  if (!date) return "";
  date = date.toString();
  return moment(date).format("MMMM Do, YYYY");
});
