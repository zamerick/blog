export function logError(error) {
  if (error.response) {
    console.log("Data:", error.response.data);
    console.log("Status:", error.response.status);
    console.log("Headers:", error.response.headers);
  } else {
    console.log("Error", error.message);
  }
  console.log("Config:", error.config);
}

export function loadView(component) {
  return () => import(`@/views/${component}.vue`);
}
export function loadComponent(component) {
  return () => import(`@/components/${component}.vue`);
}
