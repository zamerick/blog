import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";
import { logError } from "@/util";
Vue.use(Vuex);
const API_URL = "https://api.zamerick.com/wp-json/wp/v2";
export default new Vuex.Store({
  state: {
    openCommentForm: false,
    posts: [],
    users: [],
    tags: [],
    categories: [],
    comments: []
  },
  actions: {
    LoadData: function({ commit }) {
      axios
        .all([
          axios.get(API_URL + "/posts"),
          axios.get(API_URL + "/users"),
          axios.get(API_URL + "/tags"),
          axios.get(API_URL + "/categories"),
          axios.get(API_URL + "/comments")
        ])
        .then(
          ([
            { data: postData },
            { data: userData },
            { data: tagData },
            { data: categoryData },
            { data: commentData }
          ]) => {
            commit("setPosts", { postData: postData });
            commit("setUsers", { userData: userData });
            commit("setTags", { tagData: tagData });
            commit("setCategories", { categoryData: categoryData });
            commit("setComments", { commentData: commentData });
          }
        )
        .catch(error => logError(error));
    },
    createComment: function({ commit }, data) {
      return axios
        .post(API_URL + "/comments", data, {
          headers: {
            "Content-Type": "application/json"
          }
        })
        .then(response => {
          commit("addComment", { newComment: response.data });
          commit("toggleOpenCommentForm", true);
        })
        .catch(error => logError(error));
    },
    toggleForm: function({ commit }, oldstate) {
      commit("toggleOpenCommentForm", oldstate);
    }
  },
  mutations: {
    setPosts: (state, { postData }) => {
      state.posts = postData;
    },
    setTags: (state, { tagData }) => {
      state.tags = tagData;
    },
    setCategories: (state, { categoryData }) => {
      state.categories = categoryData;
    },
    setUsers: (state, { userData }) => {
      state.users = userData;
    },
    setComments: (state, { commentData }) => {
      state.comments = commentData;
    },
    addComment: (state, { newComment }) => {
      state.comments.push(newComment);
      state.comments = state.comments.sort((a, b) => {
        return new Date(b.date) - new Date(a.date);
      });
    },
    toggleOpenCommentForm: (state, newValue) => {
      state.openCommentForm = !newValue;
    }
  },
  getters: {
    getUserById: state => id => {
      return state.users.find(user => user.id === id);
    },

    getTagById: state => id => {
      return state.tags.find(tag => tag.id === id);
    },

    getCategoryById: state => id => {
      return state.categories.find(category => category.id === id);
    },

    getCommentById: state => id => {
      return state.comments.find(comment => comment.id === id);
    },

    getPostById: state => id => {
      return state.posts.find(post => post.id == id);
    },
    getCommentsByPostId: state => id => {
      if (state.comments.length > 0) {
        return state.comments.filter(comment => comment.post == id);
      }
    }
  },
  modules: {}
});
