import Vue from "vue";
import Router from "vue-router";
export function loadView(component) {
  return () => import(`@/views/${component}.vue`);
}
Vue.use(Router);

export default new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      name: "home",
      component: loadView("Feed")
    },
    {
      path: "/posts/:id",
      name: "single",
      component: loadView("Single"),
      props: true
    }
  ]
});
